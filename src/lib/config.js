// @flow
'use strict'

const {
  NODE_PORT,
  NODE_ENV,
  MONGO_URL
} = process.env

const port: number = NODE_PORT ? Number(NODE_PORT) : 3000
const isProduction: boolean = NODE_ENV === 'production'
const mongoUrl: string = MONGO_URL || 'mongodb://localhost/adys'

module.exports = {
  port,
  isProduction,
  mongoUrl
}
