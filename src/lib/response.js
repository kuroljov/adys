// @flow
'use strict'

type responseType = {
  ok: boolean,
  stack: Object,
  errors?: Array<string>,
  data?: Object
}

type Stack = Object

const txt = {
  PARSE_URL: 'Parse Url',
  COUNTRIES_CHECK: 'Existing Countries',
  CATEGORIES_CHECK: 'Existing Categories',
  BASE_TARGETING: 'Base Targeting',
  BUDGET_CHECK: 'Budget Check',
  BASEBID_CHECK: 'BaseBid Check',
  UPDATE_BUDGET: 'Update Budget'
}

const defaultStack: Stack = {}

Object.keys(txt).forEach(key => {
  const value = txt[key]
  defaultStack[value] = null
})

const makeStack = (stack: Stack): Stack => Object.assign({}, defaultStack, stack)

const response = (args: responseType): responseType => Object.assign({}, args, {
  stack: makeStack(args.stack)
})

module.exports = {
  response,
  txt
}
