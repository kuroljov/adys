// @flow
'use strict'

const t = require('assert')
const { ObjectId } = require('mongodb')
const Model = require('./Model')

type args = {
  _id?: ObjectId,
  name: string,
  countries: Array<ObjectId>,
  budget: number,
  bid: number,
  categories: Array<ObjectId>
}

class Company extends Model {
  name: string
  countries: Array<ObjectId>
  budget: number
  bid: number
  categories: Array<ObjectId>

  constructor (args: args) {
    super(args)

    t.ok(Company.isValid(args))

    this.name = args.name
    this.countries = args.countries
    this.budget = args.budget
    this.bid = args.bid
    this.categories = args.categories
  }

  static isValid ({ name, countries, budget, bid, categories } = {}): boolean {
    return !!name &&
      Model.isString(name) &&
      Model.isObjectIdArray(countries) &&
      Model.isNumber(budget) &&
      Model.isNumber(bid) &&
      Model.isObjectIdArray(categories)
  }
}

module.exports = Company
