// @flow
'use strict'

const t = require('assert')
const { ObjectId } = require('mongodb')
const Model = require('./Model')

type args = {
  _id?: ObjectId,
  name: string
}

class Country extends Model {
  name: string

  constructor (args: args) {
    super(args)

    t.ok(Country.isValid(args))

    this.name = args.name
  }

  static isValid ({ name } = {}): boolean {
    return !!name && Model.isString(name)
  }
}

module.exports = Country
