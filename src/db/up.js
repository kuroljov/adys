// @flow
'use strict'

const t = require('assert')
const { MongoClient } = require('mongodb')
const config = require('../lib/config')
const Category = require('../models/Category')
const Country = require('../models/Country')
const Company = require('../models/Company')

main()

const Automobile = 'Automobile'
const Finance = 'Finance'
const IT = 'IT'

const US = 'US'
const FR = 'FR'
const IN = 'IN'
const RU = 'RU'
const AU = 'AU'

async function main () {
  console.time('Finish in:')
  console.log(config.mongoUrl)
  console.log()

  const db = await MongoClient.connect(config.mongoUrl)

  const $companies = db.collection('companies')
  const $categories = db.collection('categories')
  const $countries = db.collection('countries')

  const del = await db.dropDatabase()
  t.ok(del)

  const categoriesMap: Map<string, Category> = new Map([
    [Automobile, new Category({ name: Automobile })],
    [Finance, new Category({ name: Finance })],
    [IT, new Category({ name: IT })]
  ])

  const countriesMap: Map<string, Country> = new Map([
    [US, new Country({ name: US })],
    [FR, new Country({ name: FR })],
    [IN, new Country({ name: IN })],
    [RU, new Country({ name: RU })],
    [AU, new Country({ name: AU })]
  ])

  const companies = [
    new Company({
      name: 'C1',
      budget: 1,
      bid: 0.1,
      countries: [
        countriesMap.get(US)._id,
        countriesMap.get(FR)._id
      ],
      categories: [
        categoriesMap.get(Automobile)._id,
        categoriesMap.get(Finance)._id
      ]
    }),
    new Company({
      name: 'C2',
      budget: 2,
      bid: 0.3,
      countries: [
        countriesMap.get(US)._id,
        countriesMap.get(IN)._id
      ],
      categories: [
        categoriesMap.get(IT)._id,
        categoriesMap.get(Finance)._id
      ]
    }),
    new Company({
      name: 'C3',
      budget: 3,
      bid: 0.05,
      countries: [
        countriesMap.get(US)._id,
        countriesMap.get(RU)._id
      ],
      categories: [
        categoriesMap.get(IT)._id,
        categoriesMap.get(Automobile)._id
      ]
    })
  ]

  const countries: Array<Country> = [...countriesMap].map(x => x[1])
  const categories: Array<Category> = [...categoriesMap].map(x => x[1])

  const a = await $countries.insertMany(countries)
  checkInsertion(a, countries, 'countries')

  const b = await $categories.insertMany(categories)
  checkInsertion(b, categories, 'categories')

  const c = await $companies.insertMany(companies)
  checkInsertion(c, companies, 'companies')

  const countriesCount = await $countries.count()
  t.strictEqual(countriesCount, countries.length)

  const categoriesCount = await $categories.count()
  t.strictEqual(categoriesCount, categories.length)

  const companiesCount = await $companies.count()
  t.strictEqual(companiesCount, companies.length)

  db.close()

  console.log()
  console.timeEnd('Finish in:')
}

function checkInsertion ({ result }, items, name) {
  const len = items.length

  t.ok(result.ok)
  t.strictEqual(result.n, len)
  console.log('Add %d %s', len, name)
}
