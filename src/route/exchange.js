// @flow
'use strict'

const Decimal = require('decimal.js')
const Company = require('../models/Company')
const Country = require('../models/Country')
const Category = require('../models/Category')
const parseUrl = require('../lib/parseUrl')
const { response, txt } = require('../lib/response')

const updateStack = (stack: Object, stage: string, value: boolean | Object = true) =>
  Object.assign(stack, { [stage]: value })

exports.get = async (x: Object) => {
  const url = parseUrl(x.url)
  const stack = {}

  if (url.errors) {
    x.body = response({
      ok: false,
      errors: url.errors,
      stack: {
        [txt.PARSE_URL]: false
      }
    })
    return
  }

  updateStack(stack, txt.PARSE_URL)

  if (!url.data) {
    return x.throw(500)
  }

  const countryNames: Array<string> = url.data.countries
  const categoryNames: Array<string> = url.data.categories
  const baseBid: number = url.data.bid

  const countries: Array<Country> = await x.$countries.find({
    name: { $in: countryNames }
  }).toArray()

  if (!countries.length) {
    x.body = response({
      ok: false,
      stack: Object.assign({}, stack, {
        [txt.COUNTRIES_CHECK]: false
      })
    })
    return
  }

  updateStack(stack, txt.COUNTRIES_CHECK)

  const categories: Array<Category> = await x.$categories.find({
    name: { $in: categoryNames }
  }).toArray()

  if (!categories.length) {
    x.body = response({
      ok: false,
      stack: Object.assign({}, stack, {
        [txt.CATEGORIES_CHECK]: false
      })
    })
    return
  }

  updateStack(stack, txt.CATEGORIES_CHECK)

  const companies: Array<Company> = await x.$companies.find().toArray()

  const makePassed: Function = (base: Array<Company>) =>
    companies.reduce((obj: Object, company: Company) => {
      obj[company.name] = base.includes(company)
      return obj
    }, {})

  const matchCompanies: Array<Company> = companies
    .filter((company: Company) => company.countries.some(id => countries.map(x => x._id.toString()).includes(id.toString())))
    .filter((company: Company) => company.categories.some(id => categories.map(x => x._id.toString()).includes(id.toString())))

  updateStack(stack, txt.BASE_TARGETING, makePassed(matchCompanies))

  if (!matchCompanies.length) {
    x.body = response({
      ok: false,
      stack
    })
    return
  }

  const companiesWithBudget: Array<Company> = matchCompanies
    .filter((company: Company) => company.budget >= baseBid)

  updateStack(stack, txt.BUDGET_CHECK, makePassed(companiesWithBudget))

  if (!companiesWithBudget.length) {
    x.body = response({
      ok: false,
      stack
    })
    return
  }

  const companiesWithHigherBid: Array<Company> = companiesWithBudget
    .filter((company: Company) => company.bid >= baseBid)

  updateStack(stack, txt.BASEBID_CHECK, makePassed(companiesWithHigherBid))

  if (!companiesWithHigherBid.length) {
    x.body = response({
      ok: false,
      stack
    })
    return
  }

  const winner: Company = companiesWithHigherBid.length > 1
    ? companiesWithHigherBid.sort((a: Company, b: Company) => b.bid - a.bid)[0]
    : companiesWithHigherBid[0]

  winner.budget = +Decimal(winner.budget).minus(baseBid)

  const updateCompany = await x.$companies.updateOne({
    _id: winner._id
  }, {
    $set: {
      budget: winner.budget
    }
  })

  if (!updateCompany.result.ok || updateCompany.result.nModified !== 1) {
    x.body = response({
      ok: false,
      stack: Object.assign({}, stack, {
        [txt.UPDATE_BUDGET]: false
      })
    })
    return
  }

  updateStack(stack, txt.UPDATE_BUDGET)

  x.body = response({
    ok: true,
    stack,
    data: {
      winner: {
        name: winner.name,
        budget: winner.budget,
        bid: winner.bid
      }
    }
  })
}
