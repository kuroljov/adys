# Adys

> Mini Exchange App

### Get started

You need just one command here

```bash
docker-compose up
```

Then you are welcome to open browser on port `3000`
